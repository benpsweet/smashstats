Meteor.methods({
	'fighterSelectData': function(){
		var rawFighterData = Fighters.find({}, { sort: {name: 1} }).fetch()
		var fighterData = []

		for (x=0; x < rawFighterData.length; x++) {
			fighterData.push( { id: rawFighterData[x]._id,
								text: rawFighterData[x].name,
								slug: rawFighterData[x].slug
							} )
		}

		return fighterData
	},
});