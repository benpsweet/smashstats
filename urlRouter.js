Router.configure({
  layoutTemplate: 'smash_stats',
});

Router.route('/', function() { 
  this.render()
}, {
  name: 'home',
  template: 'home'
});

Router.route('/about/', function() { 
  this.render()
}, {
  name: 'about',
  template: 'about'
});

Router.route('/fighters/', function() { 
  this.render()
}, {
  name: 'fighterList',
  template: 'fighterList'
});