function formatFighter (fighter) {
	if (fighter.slug){
		return $('<span><img src="/images/characters/stockicon/' + fighter.slug + '.png" style="width:32px;height:32px;" /> ' + fighter.text + '</span>')
	}
	return $('<span>' + fighter.text + '</span>')
};

function createFighterSelect (target) {
	var fighterData

	Meteor.call('fighterSelectData', function(error, result){
		if (error)
		{
			console.log(error)
		}
		else
		{
			$(target).select2({
				data: result,
				templateResult: formatFighter,
				templateSelection: formatFighter,
				placeholder: "Select Fighter",
				allowClear: true
			})
		}
	})
}

Template.fighterList.helpers({
	'fighter': function(){
		return Fighters.find({}, { sort: {name: 1} })
	},
});

Template.addMatches.events({
	'click .match-spinners button': function(e){
		console.log('CLICKT')
		console.log(e)
		console.log(e.currentTarget.parentElement.children)
		xyz = e.currentTarget.parentElement
	},

	'click #addMatch': function(){

	},

	'keypress .spinner-input': function(e){
		//if (e.charCode > 57 || e.charCode < 48 || parseInt(String.fromCharCode(e.charCode)) > 2)
		///	e.preventDefault()
	},
});

Template.addMatches.rendered = function(){
	createFighterSelect('#playerCharacter')
	createFighterSelect('#opponentCharacter')

	$('#matchDate').datepicker({
		todayBtn: "linked",
		todayHighlight: true,
		setDate: new Date(),
	});
	$('#matchDate').datepicker('update',new Date());
}