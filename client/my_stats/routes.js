Router.route('/my_stats/', function() { 
  this.render()
}, {
  name: 'myStats',
  template: 'myStats'
});

Router.route('/my_stats/add_matches', function() { 
  this.render()
}, {
  name: 'addMatches',
  template: 'myStats',
  yieldTemplates: {
      'addMatches': {to: 'myStatsContent'},
    }
});